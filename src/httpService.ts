import {Request, Response} from 'express';

type RemoteServiceRequest = Request<{}, any, {
    action: string,
    payload: any[],
}>

type ServiceFunction = (...args: any[]) => Promise<any>
type Service = Record<string, ServiceFunction>
type AsService<T> = {
    [K in Extract<keyof T, string>]: T[K] extends ServiceFunction ? T[K] : never
}

class HttpServiceHandler<T extends Service> {
    private readonly service: Readonly<T>;

    public constructor(service: T) {
        this.service = Object.freeze({
            ...this.getPrototypeActions(service),
            ...this.getOwnActions(service),
        } as T);
        this.entrypoint = this.entrypoint.bind(this);
    }

    public entrypoint(req: RemoteServiceRequest, res: Response): void {
        const {action, payload} = req.body;
        const serviceFunction = this.service[action];
        if (serviceFunction === undefined) {
            res.status(500).json({
                message: 'Invalid action.',
            });
            return;
        }

        serviceFunction(...payload)
            .then(result => res.status(200).json(result === undefined ? '' : result))
            .catch(error => res.status(500).json({
                message: error.message,
            }))
            .then(() => undefined);
    }

    private getPrototypeActions(service: T): Service {
        const proto = Object.getPrototypeOf(service);
        return Object.fromEntries(
            Object.getOwnPropertyNames(proto)
                .filter(name => name !== 'constructor')
                .map<[string, ServiceFunction]>(name => [name, proto[name]])
                .map(([name, serviceFn]) => [name, serviceFn.bind(service)]),
        );
    }

    private getOwnActions(service: Service): Service {
        return Object.fromEntries(
            Object.getOwnPropertyNames(service)
                .map<[string, ServiceFunction]>(name => [name, service[name] as ServiceFunction])
                .map(([name, serviceFn]) => [name, serviceFn.bind(service)]),
        );
    }
}

export function bootstrapHttpService<T>(service: AsService<T>): (req: RemoteServiceRequest, rea: Response) => void {
    const handler = new HttpServiceHandler(service);
    const returned = handler.entrypoint;
    (returned as any).fullHandler = handler;
    return returned;
}
