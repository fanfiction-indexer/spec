import {AxiosInstance, AxiosResponse} from 'axios';

export class BaseRemoteService {
    public constructor(
        protected readonly client: AxiosInstance,
        protected readonly url: string,
    ) {}

    protected request<R, P extends Array<any> = any[]>(action: string, payload: P): Promise<R> {
        return this.client.post(this.url, {
            action: action,
            payload: payload,
        })
            .then((res: AxiosResponse<R>) => {
                if (200 <= res.status && res.status < 300) {
                    const response = <any>res.data === '' ? undefined : res.data;
                    return response as R
                }
                throw new Error(<any>res.data);
            });
    }
}
