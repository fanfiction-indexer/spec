import {Fandom} from '../domain';
import {BaseRemoteService} from './remoteBase';

export interface StarterService {
    startIndexing(fandom: Fandom): Promise<void>;
}

export class RemoteStarterService extends BaseRemoteService implements StarterService {
    public startIndexing(fandom: Fandom): Promise<void> {
        return this.request('startIndexing', [fandom]);
    }
}
