module.exports = {
    testMatch: [
        '<rootDir>/test/**/*.test.ts',
    ],
    transform: {
        '^.+\\.ts$': 'ts-jest',
    },
    coveragePathIgnorePatterns: [
        '/node_modules/',
        '/test/',
        '<rootDir>/src/infrastructure/entrypoint/',
    ],
};
